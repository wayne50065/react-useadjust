## Get started

```
    npm i react-adjust
```

## CodeSandbox

https://codesandbox.io/s/adoring-thunder-bop5dq?file=/src/App.js:0-2862

## Code Example

```
import React, { useState } from "react";
import { useAdjust } from "react-adjust";
export default function Demo() {
  const {
    VerticalResizeEdge,
    HorizontalResizeEdge,
    heights,
    widths
  } = useAdjust();
  const [resizeHeight, setResizeHeight] = useState({});
  const [resizeWidths, setResizeWidths] = useState({});

  React.useEffect(() => {
    setResizeWidths(widths);
    setResizeHeight(heights);
  }, [heights, widths]);
  return (
    <div className="container">
      <div
        style={{
          backgroundColor: "grey",
          width: "90vw",
          height: "90vh",
          display: "flex",
          justifyContent: "center",
          padding: "20px",
          alignItems: "center"
        }}
      >
        <div
          id="horizontal-resize"
          style={{
            width: "100%",
            height: "100%",
            background: "orange",
            display: "flex"
          }}
        >
          <div
            style={{
              flex:
                Object.keys(resizeWidths).length > 0
                  ? resizeWidths["horizontal-resize"].left
                  : 1,
              background: "#993"
            }}
          ></div>
          <HorizontalResizeEdge
            target="horizontal-resize"
            initWidth="40%"
            limits={{ left: "50", right: "200" }}
          >
            adjust me
          </HorizontalResizeEdge>
          <div
            style={{
              flex:
                Object.keys(resizeWidths).length > 0
                  ? resizeWidths["horizontal-resize"].right
                  : 1,
              background: "#193",
              margin: "20px",
              display: "flex",
              flexDirection: "column"
            }}
            id="vertical-resize"
          >
            <div
              style={{
                flex:
                  Object.keys(resizeHeight).length > 0
                    ? resizeHeight["vertical-resize"].upper
                    : 1,
                background: "#837"
              }}
              className="verticalResize-box"
            >
              <div style={{ height: "48px" }}></div>
            </div>
            <div
              style={{
                flex:
                  Object.keys(resizeHeight).length > 0
                    ? resizeHeight["vertical-resize"].lower
                    : 1,
                background: "grey",
                position: "relative"
              }}
              className="verticalResize-box"
            >
              <VerticalResizeEdge
                target="vertical-resize"
                initHeight="80%"
                limits={{ top: "48", bottom: "48" }}
              >
                adjust me
              </VerticalResizeEdge>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

```
